﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace lab2Alg
{
	class PyramidSorting
	{
		//add 1 element to the pyramid
		static int Add2pyramid(int[] arr, int i, int N)
		{
			int imax;
			if ((2 * i + 2) < N)
			{
				if (arr[2 * i + 1] > arr[2 * i + 2]) imax = 2 * i + 2;
				else imax = 2 * i + 1;
			}
			else imax = 2 * i + 1;
			if (imax >= N) return i;
			if (arr[i] > arr[imax])
			{
				Swaper.Swap(ref arr[i], ref arr[imax]);
				if (imax < N / 2) i = imax;
			}
			return i;
		}

		public static int[] Sort(int[] mas, int len)
		{
			var arr = new int[len];
			mas.CopyTo(arr, 0);
			//step 1: building the pyramid
			for (int i = len / 2 - 1; i >= 0; --i)
			{
				long prev_i = i;
				i = Add2pyramid(arr, i, len);
				if (prev_i != i) ++i;
				Swaper.Iterat();
			}

			//step 2: sorting
			for (int k = len - 1; k > 0; --k)
			{
				Swaper.Swap(ref arr[0], ref arr[k]);
				int i = 0, prev_i = -1;
				while (i != prev_i)
				{
					prev_i = i;
					i = Add2pyramid(arr, i, k);
					Swaper.Iterat();
				}
			}

			return arr;
		}
	}

	class InsertSorting
	{
		public static int[] Sort(int countel, int[] oldmas)
		{
			var mas = new int[countel];
			oldmas.CopyTo(mas, 0);

			for (int i = 1; i < countel; i++)
			{
				for (int j = i; j > 0; j--)
				{
					if (mas[j - 1] < mas[j])
					{
						Swaper.Swap(ref mas[j - 1], ref mas[j]);
					}
					Swaper.Iterat();
				}
			}

			return mas;
		}
	}

	class SelectionSorting
	{
		public static int[] Sort(int[] mas)
		{
			var list = new int[mas.Length];
			mas.CopyTo(list, 0);
			for (int i = 0; i < list.Length - 1; i++)
			{
				int min = i;

				for (int j = i + 1; j < list.Length; j++)
				{
					if (list[j] > list[min])
					{
						min = j;
					}
					Swaper.Iterat();
				}

				Swaper.Swap(ref list[i], ref list[min]);
			}

			return list;
		}
	}

	class ShakerSorting
	{
		public static int[] Sort(int[] mas)
		{
			var myint = new int[mas.Length];
			mas.CopyTo(myint, 0);

			int left = 0,
				right = myint.Length - 1,
				count = 0;

			while (left < right)
			{
				for (int i = left; i < right; i++)
				{
					count++;
					if (myint[i] < myint[i + 1])
						Swaper.Swap(myint, i, i + 1);
					Swaper.Iterat();
				}
				right--;

				for (int i = right; i > left; i--)
				{
					count++;
					if (myint[i - 1] < myint[i])
						Swaper.Swap(myint, i - 1, i);
					Swaper.Iterat();
				}
				left++;
			}

			return myint;
		}
	}

	class QSorting1
	{
		public static int[] Sort(int[] mas, int firstIndex = 0, int lastIndex = -1)
		{
			var array = new int[mas.Length];
			mas.CopyTo(array, 0);

			return QSort(array, 0, array.Length - 1);
		}

		private static int[] QSort(int[] mas, int left, int right)
		{
			if (left >= right)
				return mas;

			int leftTemp = left;
			int rightTemp = right;
			int pi = (leftTemp + rightTemp) / 2;
			int p = mas[pi]; // Опорным элементом для примера возьмём средний

			while (leftTemp <= rightTemp)
			{
				while (mas[leftTemp] < p)
				{
					leftTemp++;
					Swaper.Iterat();
				}
				while (mas[rightTemp] > p)
				{
					rightTemp--;
					Swaper.Iterat();
				}

				if (leftTemp <= rightTemp)
					Swaper.Swap(ref mas[leftTemp++], ref mas[rightTemp--]);
			}

			if (left < rightTemp) QSort(mas, left, rightTemp);
			if (right > leftTemp) QSort(mas, leftTemp, right);

			return mas;
		}
	}

	class QSorting2
	{
		public static int[] Sort(int[] mas, int firstIndex = 0, int lastIndex = -1)
		{
			var array = new int[mas.Length];
			mas.CopyTo(array, 0);

			return QSort(array, 0, array.Length - 1);
		}

		private static int[] QSort(int[] mas, int left1, int right2)
		{
			if (left1 >= right2)
				return mas;

			int n = right2 - left1;
			int pi1 = left1 + n / 3;
			int pi2 = right2 - n / 3;

			// ===========================================

			if (mas[pi1] > mas[pi2])
				Swaper.Swap(ref mas[pi1], ref mas[pi2]);

			int p1 = mas[pi1];
			int p2 = mas[pi2];

			int right1 = pi2 - 1;
			int leftTemp1 = left1;
			int rightTemp1 = right1;
			SwapAreas(mas, ref leftTemp1, ref rightTemp1, p1);

			if (left1 < rightTemp1) QSort(mas, left1, rightTemp1);
			if (leftTemp1 < right1) QSort(mas, leftTemp1, right1);

			// ===========================================
			if (mas[pi1] > mas[pi2])
				Swaper.Swap(ref mas[pi1], ref mas[pi2]);

			p1 = mas[pi1];
			p2 = mas[pi2];

			int left2 = pi1 + 1;
			int leftTemp2 = left2;
			int rightTemp2 = right2;
			SwapAreas(mas, ref leftTemp2, ref rightTemp2, p2);

			if (left2 < rightTemp2) QSort(mas, left2, rightTemp2);
			if (leftTemp2 < right2) QSort(mas, leftTemp2, right2);
			// ===========================================

			if (mas[pi1] > mas[pi2])
				Swaper.Swap(ref mas[pi1], ref mas[pi2]);

			p1 = mas[pi1];
			p2 = mas[pi2];

			right1 = pi2 - 1;
			leftTemp1 = left1;
			rightTemp1 = right1;
			SwapAreas(mas, ref leftTemp1, ref rightTemp1, p1);

			if (left1 < rightTemp1) QSort(mas, left1, rightTemp1);
			if (leftTemp1 < right1) QSort(mas, leftTemp1, right1);
			// ===========================================

			return mas;
		}

		private static void SwapAreas(int[] mas, ref int leftTemp, ref int rightTemp, int p)
		{
			while (leftTemp <= rightTemp)
			{
				while (mas[leftTemp] < p)
				{
					leftTemp++;
					Swaper.Iterat();
				}
				while (mas[rightTemp] > p)
				{
					rightTemp--;
					Swaper.Iterat();
				}

				if (leftTemp <= rightTemp)
					Swaper.Swap(ref mas[leftTemp++], ref mas[rightTemp--]);
			}
		}
	}

	class Swaper
	{
		public static ulong Iterations { get; private set; }
		public static ulong Count { get; private set; }

		public static void Iterat()
		{
			Iterations++;
		}

		public static void Swap(ref int a, ref int b)
		{
			int tmp = a;
			a = b;
			b = tmp;

			Count++;
		}

		public static void Swap(int[] myint, int i, int j)
		{
			int glass = myint[i];
			myint[i] = myint[j];
			myint[j] = glass;

			Count++;
		}

		public static void Reset()
		{
			Iterations = 0;
			Count = 0;
		}
	}

	public class Program
	{
		static Random rand = new Random();

		public static void Main(string[] args)
		{
			Console.Write("Input size of massiv: ");
			var size = int.Parse(Console.ReadLine());
			var mas = Geberate(size);

			//Print(mas); Console.WriteLine("not sorting");

			var newmas = InsertSorting.Sort(size, mas);             // вставкой
																	/*Print(newmas);*/
			Console.WriteLine("inserting\t: iterations {0}\t: swap {1}", Swaper.Iterations, Swaper.Count); Swaper.Reset();

			var newmas2 = SelectionSorting.Sort(mas);               // выбором
																	/*Print(newmas2);*/
			Console.WriteLine("selection\t: iterations {0}\t: swap {1}", Swaper.Iterations, Swaper.Count); Swaper.Reset();

			var newmas3 = ShakerSorting.Sort(mas);                  // обменом (шейкреная)
																	/*Print(newmas3);*/
			Console.WriteLine("shaker\t\t: iterations {0}\t: swap {1}", Swaper.Iterations, Swaper.Count); Swaper.Reset();

			var newmas4 = QSorting1.Sort(mas);                       // сортировка с помощью разделения(быстрая сортировка)
																	 /*Print(newmas4);*/
			Console.WriteLine("quick\t\t: iterations {0}\t: swap {1}", Swaper.Iterations, Swaper.Count); Swaper.Reset();

			var newmas5 = QSorting2.Sort(mas);                       // сортировка с помощью разделения(быстрая сортировка V2)
																	 /*Print(newmas5);*/
			Console.WriteLine("quick V2\t: iterations {0}\t: swap {1}", Swaper.Iterations, Swaper.Count); Swaper.Reset();

			var newmas6 = PyramidSorting.Sort(mas, mas.Length);     // пирамидальная
																	/*Print(newmas6);*/
			Console.WriteLine("pyramid\t\t: iterations {0}\t: swap {1}", Swaper.Iterations, Swaper.Count); Swaper.Reset();

			//var mas = new int[]
			//{
			//	//14, 9, 7, 6, 11, 3, 8, 16, 4, 1, 13, 2, 5, 10, 12, 15, 0
			//	//0, 6, 2, 3, 12, 11, 1, 7, 8, 9, 10, 5, 4, 13, 14, 15, 16
			//	16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
			//};

			//var newMas = QSorting2.Sort(mas);
			//PrintLine(newMas);
		}

		public static int[] Geberate(int size)
		{
			int[] mas = new int[size];

			for (var i = 0; i < size; i++)
			{
				mas[i] = rand.Next(-123, 890);
			}

			return mas;
		}

		public static void Print(int[] mas)
		{
			foreach (var item in mas)
			{
				Console.Write("{0} ", item);
			}
		}

		public static void PrintLine(int[] mas)
		{
			Print(mas);
			Console.WriteLine();
		}
	}
}